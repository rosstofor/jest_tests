const allData = require('../all')

test('Check if array contains 6 element (1st)', () => {
  expect(allData.length).toBe(6)
})
test('Check if array contains 6 element (2st)', () => {
  expect(allData).toHaveLength(6)
})
test('Check if array contains 6 element', () => {
  expect('strstr').toHaveLength(6)
})
test('Check if array not contains number 25', () => {
  expect(allData).not.toContain(25)
})
test('Check if array not contains number zero', () => {
  for (let i = 0; i < allData.length; i++) {
    expect(allData[i]).not.toBe(0)
  }
})
test('Check if array contains only numbers (1st)', () => {
  for (let i = 0; i < allData.length; i++) {
    expect(isNaN(allData[i])).toBe(false)
  }
})
test('Check if array contains only numbers (2nd)', () => {
  for (let i = 0; i < allData.length; i++) {
    expect(isNaN(allData[i])).toBeFalsy()
  }
})
test('Check if array contains only numbers (3rd)', () => {
  for (let i = 0; i < allData.length; i++) {
    expect(isNaN(allData[i])).not.toBeTruthy()
  }
})
test('Check if first element of array is larger than 1 or equal', () => {
  expect(allData[0]).toBeGreaterThanOrEqual(1)
})
test('Check if first element of array is less than 6 or equal', () => {
  expect(allData[allData.length - 1]).toBeLessThanOrEqual(6)
})
test('CHeck for undefined', () => {
  let a
  expect(a).toBeUndefined()
})
test('Check for substring inside string by regex', () => {
  expect('My name is Oleg').toMatch(/Oleg/)
})
test('Check for substring inside string by regex', () => {
  expect('My name is Oleg').not.toMatch(/oleg/)
})
test('Check for property', () => {
  let obj = {
    name: 'Oleg',
    age: 27,
  }
  expect(obj).toHaveProperty('age')
})
test('Check for property age id 28', () => {
  let obj = {
    name: 'Oleg',
    age: 28,
  }
  expect(obj).toHaveProperty('age', 28)
})
