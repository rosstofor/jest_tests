test('Expect anything', () => {
  let a = null
  let b
  let c = NaN
  expect(20).toEqual(expect.anything())
  expect('Anything').toEqual(expect.anything())
  expect([1, 2, 3]).toEqual(expect.anything())
  ///////////////////////////////
  expect(a).not.toEqual(expect.anything())
  expect(b).not.toEqual(expect.anything())
  ///////////////////////////////
  expect(c).toEqual(expect.anything())

  // Anything but undefined and null
})

// expect.any(Constructor)

test('Expect any constructor', () => {
  expect(10).toEqual(expect.any(Number))
})
test('Expect any constructor', () => {
  expect('10').toEqual(expect.any(String))
})

// expect.arrayContaining(array)

test('Expect array to be found to main array', () => {
  expect([1, 2, 3, 4, 5, 6]).toEqual(expect.arrayContaining([6, 2]))
})
