const filterInput = require('../input')

describe('Validate input filed', () => {
  test('Check for empty field', () => {
    expect(filterInput()).toBe('Unknown')
  })
  test('Check for spaces in start and end', () => {
    expect(filterInput(' Oleg ')).toBe('Oleg')
    expect(filterInput('    Oleg  ')).toBe('Oleg')
  })
  test('Check if name length > 10 characters', () => {
    expect(filterInput('Oleg_Marchenko')).toBe('Oleg_March')
    expect(filterInput('          Oleg_Marchenko')).toBe('Oleg_March')
  })
  test('Check if name starts with underscores', () => {
    expect(filterInput('_Oleg')).toBe('Oleg')
    expect(filterInput('___Oleg')).toBe('Oleg')
    expect(filterInput('     ___Oleg_Marchenko')).toBe('Oleg_March')
  })
})
