const mock = require('../mock')

test('Mock functions', () => {
  const mocker = jest.fn(mock)
  expect(mocker('Oleg')).toBe('Hello Oleg')
  expect(mocker('Oleg2')).toBe('Hello Oleg2')
  expect(mocker('Oleg3')).toBe('Hello Oleg3')
  expect(mocker).toHaveBeenCalled()
  expect(mocker).toHaveBeenCalledTimes(3)
  expect(mocker).toHaveBeenLastCalledWith('Oleg3')
})
