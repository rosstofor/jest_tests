expect.extend({
  personalToBeLargerThan(received, target) {
    const res = received > target
    if (res) {
      return {
        message: () => `Expected ${received} to be larger than ${target}`,
        pass: true,
      }
    } else {
      return {
        message: () => `Error: Expected ${received} to be larger than ${target}`,
        pass: false,
      }
    }
  },
})

test('Check personal test', () => {
  expect(1).not.personalToBeLargerThan(8)
})
test('Check personal test', () => {
  expect(21).personalToBeLargerThan(8)
})

expect.extend({
  toBebetween(received, start, end) {
    const pass = received > start && received < end
    if (pass) {
      return {
        message: () => `Expected ${received} to be between ${start} and ${end}`,
        pass: true,
      }
    } else {
      return {
        message: () => `Expected ${received} to be between ${start} and ${end}`,
        pass: false,
      }
    }
  },
})

test('Check personal test toBeBetween', () => {
  expect(20).toBebetween(15, 25)
})
