const theSum = require('../sum')

// test(name, fn, timeout)
// it(name, fn, timeout)
describe('Check total sum numbers', () => {
  describe('Check if no numbers or only one', () => {
    test('Return 0 if num NULL', () => {
      expect(theSum()).toBe(0)
    })

    test('Return the number', () => {
      expect(theSum(20)).toBe(20)
    })
  })
  describe('Check more less two numbers', () => {
    test('Return the number 15 + 25', () => {
      expect(theSum(10, 25)).toBe(35)
    })

    test('Return the number 10 + 20 + 30', () => {
      expect(theSum(10, 20, 30)).toBe(60)
    })
  })
})
