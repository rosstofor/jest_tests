/**
 * filterInput function
 * [1] Check for empty field ---> Passed
 * [2] Check for spaces in start and end ---> Passed
 * [3] Check if name length > 10 characters ---> Passed
 * [4] Check if name starts with underscores ---> Passed
 */

const filterInput = (name) => {
  if (!name) return 'Unknown'
  if (name.startsWith(' ') || name.endsWith(' ')) name = name.trim() // or .trim(' ')
  if (name.startsWith('_')) {
    name = name.slice(1)
    return filterInput(name)
  }
  if (name.length > 10) name = name.slice(0, 10)
  return name
}

filterInput('   ___Oleg_Marchenko  ')

module.exports = filterInput
