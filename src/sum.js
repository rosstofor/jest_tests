/**
 *  theSum function
 * [1] Check total sum numbers ---> Passed
 * [2] Return 0 if num NULL ---> Passed
 * [3] Return the number ---> Passed
 * [4] Check more less two numbers ---> Passed
 */

const theSum = (...numbers) => numbers.reduce((prev, curr) => prev + curr, 0)

module.exports = theSum
